package controllers;
import play.mvc.Controller;
import play.mvc.Result;
import models.UserAccount;
import java.util.List;
import play.data.Form;
import play.mvc.Security;
import views.html.useraccount.list;
import views.html.useraccount.details;
import views.html.useraccount.register; 
import views.html.useraccount.updateinfo;
import views.html.useraccount.personal;


import com.avaje.ebean.Page;

public class UserCP extends Controller {
	private static final Form<UserAccount> userForm = Form.form(UserAccount.class);
	private static final Form<UserAccount> userForm_2 = Form.form(UserAccount.class);
	@Security.Authenticated(Secured.class)

	public static Result personal() {
    
        return ok(personal.render());
     }
	@Security.Authenticated(Secured.class)

	public static Result list(Integer page) {
        Page<UserAccount> UserAccountlist = UserAccount.find(page);
        return ok(list.render(UserAccountlist));
     }
	@Security.Authenticated(Secured.class)

	public static Result Register() {
		return ok(register.render(userForm));
     }
	@Security.Authenticated(Secured.class)

	public static Result edit() {

		UserAccount usr = UserAccount.findByEmail(session().get("email"));
		Form<UserAccount> filledForm = userForm_2.fill(usr);
		return ok(updateinfo.render(filledForm));
     }
	@Security.Authenticated(Secured.class)

	public static Result details(Long ID) {
		UserAccount usr = UserAccount.findById(ID);		
		return ok(details.render(usr));
	} 

	public static Result save() {
		Form<UserAccount> boundForm = userForm.bindFromRequest();
		if (boundForm.hasErrors()) {
			flash("error", "Please correct the form below.");
			return badRequest(personal.render());
		}
		UserAccount usr = boundForm.get();
		if (usr.id == null) {
            usr.save();
        } else {
            usr.update();
        }
		flash("success", String.format("Successfully added usr %s", usr));
		return redirect(routes.UserCP.list(0));

	}
	@Security.Authenticated(Secured.class)

	public static Result delete(Long ID) {
		final UserAccount usr = UserAccount .findById(ID);
		if(usr == null) {
			return notFound(String.format("usr %s does not exists.", ID));
		}
		usr.delete();
		return redirect(routes.UserCP.personal());
	}
	public static Result admin() {
		return ok(register.render(userForm));
     }
}
