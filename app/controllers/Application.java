package controllers;

import play.mvc.*;
import static play.data.Form.form;
import play.data.Form;
import views.html.*;
import views.html.system.*;
import models.UserAccount;


public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }
	
	public static class Login {
        public String email;
        public String password;
    }
	
	public static Result login() {
	       return ok(
	          login.render(form(Login.class))
	       );
	}
	public static Result  logout() {
		session().clear();
		return redirect(routes.Application.login()); 
	}
  
	public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        String email = loginForm.get().email;
        String password = loginForm.get().password;
 
        session().clear();
        if (UserAccount.authenticate(email, password) == null) {
            flash("error", "Invalid email and/or password");
            return redirect(routes.Application.login());       
        }
        UserAccount usr = UserAccount.findByEmail(email);
        session("email", email);
        session("type", usr.type+"");
 
        return redirect(routes.UserCP.personal());
    }
}
