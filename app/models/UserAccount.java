package models;
 
import play.data.validation.Constraints;
import play.db.ebean.Model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Page;
 
	@Entity
public class UserAccount extends Model {
    @Id
    public Long id;
	@Constraints.Required
    public String email;
    public String name;
    @Constraints.Required
    public String password;
    public int type;
    public String address;
    public String dateofbirth;
 
    public UserAccount() { }
    public UserAccount(String email, String password) {
        this.email = email;
        this.password = password;
    }
	public UserAccount(String email, String name, String password, int type, String address, String dateofbirth) {
        this.email = email;
		this.name = name;
        this.password = password;
        this.type = type;
		this.address = address;
		this.dateofbirth = dateofbirth;
    }
	
	public static Finder<Long, UserAccount> finder =
            new Finder<Long, UserAccount>(Long.class, UserAccount.class);
    
	public static UserAccount authenticate(String email, String password) {
        return finder.where().eq("email", email)
                             .eq("password", password).findUnique();
    }

	public static List<UserAccount> findAll() {
		return finder.all();
	}
	
	public static UserAccount findById(Long id){
		return finder.byId(id);
	}
	
	public static UserAccount findByEmail(String email) {
		return finder.where().eq("email", email).findUnique();
	}
	
	public static Page<UserAccount> find(int page) {  // trả về trang thay vì List
		   return finder.where()
		              .orderBy("id asc")     // sắp xếp tăng dần theo id
		              .findPagingList(10)    // quy định kích thước của trang
		              .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
		              .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0
	}
}